# android-m8

### Proyecto app manejo de archivos con Android  

Aplicacion con una pantalla de login cuya información se guarde en un fichero txt
**Debe haber dos botones, registrarse y validar**  

> Registrarse escribe en el fichero los nuevos datos y Validar comprueba que existan los datos inttroducidos.  

En android debemos esccoger si escribimos en la memoria interna o externa.  
## Ver documentación _Data file storage_ overview
* Memoria interna -> Fichero que genera la propia aplicación  
* Externa -> cualquier carpeta de las que son compartidas con otras aplicaciones (Download, DCIM..)  

Formulas para acceder a la memoria y crear un fichero: 
~~~
GetFilesDir 
GetExternalFilesDir
~~~

## Layouts  
* Un tutorial que nos explica paso a paso con imagenes, en castellano y de forma   
comprensible como funcionan los layouts en Android y los tipos que hay  

> http://www.hermosaprogramacion.com/2015/08/tutorial-layouts-en-android/  

## 1. Tarea asíncrona ejemplo (Primer paso para poder hacer una llamada a una API)
> https://openclassrooms.com/en/courses/4788266-integrate-remote-data-into-your-app/5292981-create-your-first-asynchronous-tasks  

## 2. Acceso a la API 
* Class *HttpURLConnection* --> Crea el objeto conexión [Android API docs](https://developer.android.com/reference/java/net/HttpURLConnection)  
* extender de *android.os.AsyncTask<String, Void, String>* [Android API docs](https://developer.android.com/reference/android/os/AsyncTask)
* Llamada de los datos en un Fragment  

## 3. Ordenación de los datos de la llamada en un RecyclerView  
* Nuestra clase extenderá de la clase *RecyclerView.ViewHolder* [Android API docs](https://developer.android.com/reference/androidx/recyclerview/widget/RecyclerView.ViewHolder?hl=es_419)  
* El view Holder no lo llamamos directamente, emplearemos un Adapter [RecyclerView.Adapter<RecyclerView.ViewHolder>](https://developer.android.com/reference/androidx/recyclerview/widget/RecyclerView.Adapter?hl=es_419)  
Esto nos permitirá enlazar el RecyclerView con nuestro controlador (MainFragment).  






